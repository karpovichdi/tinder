﻿using System.ComponentModel;
using Xamarin.Forms;

namespace Tinder
{
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}
